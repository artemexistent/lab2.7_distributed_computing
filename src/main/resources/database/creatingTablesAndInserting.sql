CREATE TABLE Employee
(
    id            bigint auto_increment primary key,
    firstName     VARCHAR(64) NOT NULL,
    secondName    VARCHAR(64) NOT NULL,
    position      VARCHAR(64) NOT NULL,
    id_department bigint      NOT NULL
);
CREATE TABLE Department
(
    id   bigint auto_increment primary key,
    name VARCHAR(64) NOT NULL
);
CREATE TABLE Project
(
    id          bigint auto_increment primary key,
    info        VARCHAR(64),
    id_customer bigint NOT NULL,
    time_start  DATE   NOT NULL,
    time_finish DATE
);
CREATE TABLE Customer
(
    id      bigint auto_increment primary key,
    company VARCHAR(64) NOT NULL,
    phone   bigint      NOT NULL
);
CREATE TABLE Manager
(
    id          bigint auto_increment,
    id_employee bigint,
    id_project  bigint
);

CREATE TABLE employee_project
(
    id          bigint auto_increment,
    id_employee bigint NOT NULL,
    id_project  bigint NOT NULL,
    time_start  DATE,
    time_left   DATE
);
CREATE TABLE time_in_company
(
    id          bigint auto_increment primary key,
    id_employee bigint NOT NULL,
    time_hire   DATE   NOT NULL,
    time_sack   DATE
);
CREATE INDEX employees_firstName_secondName_idx ON employee (firstName, secondName);
CREATE INDEX managers_id_employee_idx ON manager (id_employee);
CREATE INDEX employee_project_id_employee_idx ON employee_project (id_employee);
ALTER TABLE Employee
    ADD FOREIGN KEY (id_department)
        REFERENCES Department (id);
ALTER TABLE Project
    ADD FOREIGN KEY (id_customer)
        REFERENCES Customer (id);
ALTER TABLE Manager
    ADD FOREIGN KEY (id_employee)
        REFERENCES Employee (id);
ALTER TABLE Manager
    ADD FOREIGN KEY (id_project)
        REFERENCES Project (id) ON DELETE CASCADE;
ALTER TABLE employee_project
    ADD FOREIGN KEY (id_employee)
        REFERENCES EMPLOYEE (id);
ALTER TABLE employee_project
    ADD FOREIGN KEY (id_project)
        REFERENCES PROJECT (id);
ALTER TABLE time_in_company
    ADD FOREIGN KEY (id_employee)
        REFERENCES Employee (id);

INSERT INTO department
VALUES (1, 'Eva Platform');
INSERT INTO department
VALUES (2, 'Gambling Tech');
INSERT INTO department
VALUES (3, 'Automation and System Integration Testing');
INSERT INTO department
VALUES (4, 'Learning & Development Team');
INSERT INTO department
VALUES (5, 'Eva Integration');
INSERT INTO department
VALUES (6, 'Cross Team Shared');

INSERT INTO employee
VALUES (1, 'Anton', 'Nakonechnyi', 'Middle Java Developer', 5);
INSERT INTO employee
VALUES (2, 'Andrii', 'Ryzhenko', 'Senior Scala Developer', 1);
INSERT INTO employee
VALUES (3, 'Anna', 'Savchenko', 'Junior Project Manager', 6);
INSERT INTO employee
VALUES (4, 'Artur', 'Ashyrov', 'Head of Casino Engineering', 2);
INSERT INTO employee
VALUES (5, 'Georgii', 'Loviagin', 'Senior Scala Developer', 1);
INSERT INTO employee
VALUES (6, 'Mykola', 'Ivaniuk', 'Java Dev Lead', 5);
INSERT INTO employee
VALUES (7, 'Tetiana', 'Derkach', 'Learning & Development Team Lead', 4);
INSERT INTO employee
VALUES (8, 'Anastasiya', 'Chornous', 'Project Manager', 4);
INSERT INTO employee
VALUES (9, 'Volodymyr', 'Chyzhevskyi', 'Tech Lead', 1);
INSERT INTO employee
VALUES (10, 'Artem', 'Poltavskij', 'Trainee', 3);


INSERT INTO customer
VALUES (1, 'epam', 0995568172);
INSERT INTO customer
VALUES (2, 'dataart', 0660506052);
INSERT INTO customer
VALUES (3, 'valve', 0509700202);
INSERT INTO customer
VALUES (4, 'wezom', 0951749464);
INSERT INTO customer
VALUES (5, 'dream', 0550682734);
INSERT INTO customer
VALUES (6, 'mappa', 0658377645);

INSERT INTO time_in_company
VALUES (1, 10, '2002-10-12', '2005-11-05');
INSERT INTO time_in_company
VALUES (2, 1, '2002-9-12', null);
INSERT INTO time_in_company
VALUES (3, 2, '2002-11-12', null);
INSERT INTO time_in_company
VALUES (4, 3, '2002-4-12', null);
INSERT INTO time_in_company
VALUES (5, 4, '2005-10-12', null);
INSERT INTO time_in_company
VALUES (6, 5, '2002-10-12', null);
INSERT INTO time_in_company
VALUES (7, 6, '2003-10-12', null);
INSERT INTO time_in_company
VALUES (8, 7, '2002-10-12', null);
INSERT INTO time_in_company
VALUES (9, 8, '2004-10-12', null);
INSERT INTO time_in_company
VALUES (10, 9, '2002-10-12', null);
INSERT INTO time_in_company
VALUES (11, 10, '2006-10-12', null);


INSERT INTO project
VALUES (1, 'game', 4, '2012-12-10', '2014-12-10');
INSERT INTO project
VALUES (2, 'website', 5, '2014-12-10', null);
INSERT INTO project
VALUES (3, 'database', 2, '2013-12-10', null);
INSERT INTO project
VALUES (4, 'platform', 5, '2015-12-10', null);
INSERT INTO project
VALUES (5, 'website', 6, '2016-12-10', null);
INSERT INTO project
VALUES (6, 'website', 1, '2017-12-10', null);
INSERT INTO project
VALUES (7, 'game', 3, '2018-12-10', null);
INSERT INTO project
VALUES (8, 'game', 2, '2019-12-10', null);



INSERT INTO manager
VALUES (1, 3, 1);
INSERT INTO manager
VALUES (2, 5, 2);
INSERT INTO manager
VALUES (3, 3, 3);
INSERT INTO manager
VALUES (4, 1, 4);
INSERT INTO manager
VALUES (5, 6, 5);
INSERT INTO manager
VALUES (6, 2, 6);
INSERT INTO manager
VALUES (7, 2, 7);
INSERT INTO manager
VALUES (8, 1, 8);



INSERT INTO employee_project
VALUES (1, 4, 1, '2012-12-10', '2014-12-10');
INSERT INTO employee_project
VALUES (2, 2, 1, '2012-12-10', '2014-12-10');
INSERT INTO employee_project
VALUES (3, 1, 2, '2014-12-10', null);
INSERT INTO employee_project
VALUES (4, 6, 2, '2014-12-10', null);
INSERT INTO employee_project
VALUES (5, 2, 3, '2013-12-10', null);
INSERT INTO employee_project
VALUES (6, 5, 3, '2013-12-10', null);
INSERT INTO employee_project
VALUES (7, 2, 4, '2015-12-10', '2016-12-10');
INSERT INTO employee_project
VALUES (8, 4, 4, '2015-12-10', null);
INSERT INTO employee_project
VALUES (9, 9, 5, '2016-12-10', null);
INSERT INTO employee_project
VALUES (10, 7, 5, '2016-12-10', null);
INSERT INTO employee_project
VALUES (11, 8, 6, '2017-12-10', null);
INSERT INTO employee_project
VALUES (12, 6, 6, '2017-12-10', '2018-12-10');
INSERT INTO employee_project
VALUES (13, 9, 7, '2018-12-10', null);
INSERT INTO employee_project
VALUES (14, 7, 7, '2018-12-10', null);
INSERT INTO employee_project
VALUES (15, 9, 8, '2019-12-10', null);
INSERT INTO employee_project
VALUES (16, 8, 8, '2019-12-10', null);