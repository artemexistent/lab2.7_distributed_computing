package com.example.hw9.service.repository;

import com.example.hw9.model.database.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
  List<Project> getProjectsByCustomer_Company(String customer_company);

  List<Project> getProjectsByTimeStartIsLessThanEqualAndTimeFinishIsNull(Date timeStart);
}