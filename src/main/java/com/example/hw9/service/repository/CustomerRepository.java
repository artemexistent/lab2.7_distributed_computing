package com.example.hw9.service.repository;

import com.example.hw9.model.database.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
  Optional<Customer> findByCompany(String company);

  boolean existsByCompany(String company);
}
