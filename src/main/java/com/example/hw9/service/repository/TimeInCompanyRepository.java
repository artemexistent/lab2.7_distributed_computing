package com.example.hw9.service.repository;

import com.example.hw9.model.database.TimeInCompany;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface TimeInCompanyRepository extends JpaRepository<TimeInCompany, Long> {
  List<TimeInCompany> getTimeInCompanyByTimeHireLessThanEqualAndTimeSackNullOrTimeSackGreaterThanEqual(Date before, Date after);

  List<TimeInCompany> getTimeInCompanyByTimeSackIsNotNull();

  Optional<TimeInCompany> findTimeInCompanyByTimeSackIsNotNullAndEmployee_FirstNameAndEmployee_SecondName(String firstName, String secondName);
}
