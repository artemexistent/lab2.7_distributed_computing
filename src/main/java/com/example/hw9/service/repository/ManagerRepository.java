package com.example.hw9.service.repository;

import com.example.hw9.model.database.Manager;
import com.example.hw9.model.database.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {
  List<Manager> getManagersByEmployee_FirstNameAndEmployee_SecondName(String employee_firstName, String employee_secondName);

  List<Manager> getManagersByProjectIsIn(List<Project> projects);

  Optional<Manager> findManagerByProject_Id(Long project_id);

  boolean existsByProjectId(Long project_id);

  boolean existsByEmployee_FirstNameAndEmployee_SecondName(String employee_firstName, String employee_secondName);

  boolean existsByEmployee_FirstNameAndEmployee_SecondNameAndProject_Id(String employee_firstName, String employee_secondName, Long project_id);

}
