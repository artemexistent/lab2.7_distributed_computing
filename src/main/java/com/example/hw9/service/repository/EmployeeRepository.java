package com.example.hw9.service.repository;

import com.example.hw9.model.database.Department;
import com.example.hw9.model.database.Employee;
import com.example.hw9.model.database.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
  List<Employee> getEmployeesByDepartment(Department department);

  List<Employee> getEmployeesByDepartmentAndProjectsIsNull(Department department);

  List<Employee> getEmployeesByProjectsIsIn(List<Project> projects);

  List<Employee> getEmployeesByProjectsIsNull();

  boolean existsByFirstNameAndSecondName(String firstName, String secondName);

  boolean existsByFirstNameAndSecondNameAndProjectsIsIn(String firstName, String secondName, List<Project> projects);

  Optional<Employee> findEmployeeByFirstNameAndSecondName(String firstName, String secondName);

}
