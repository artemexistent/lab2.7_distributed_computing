package com.example.hw9.service.repository;

import com.example.hw9.model.database.Employee;
import com.example.hw9.model.database.EmployeeProjectHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.List;

public interface EmployeeProjectHistoryRepository extends JpaRepository<EmployeeProjectHistory, Long> {
  List<EmployeeProjectHistory> getEmployeeProjectByTimeStartLessThanEqualAndTimeLeftIsNullOrTimeLeftGreaterThanEqual(Date timeStart, Date timeLeft);

  List<EmployeeProjectHistory> getEmployeeProjectHistoryByTimeStartIsLessThanEqualAndTimeLeftNullOrTimeLeftIsGreaterThanEqual(Date timeStart, Date timeLeft);

  int countAllByEmployeesAndTimeLeftLessThanEqual(Employee employees, Date timeLeft);
}
