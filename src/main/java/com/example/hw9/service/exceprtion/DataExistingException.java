package com.example.hw9.service.exceprtion;

public class DataExistingException extends RuntimeException {
  public DataExistingException(String message) {
    super(message);
  }
}
