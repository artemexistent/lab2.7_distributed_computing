package com.example.hw9.service.exceprtion;

public class NoDataFoundException extends RuntimeException {
  public NoDataFoundException(String message) {
    super(message);
  }
}
