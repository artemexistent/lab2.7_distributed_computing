package com.example.hw9.service.controller;

import com.example.hw9.model.database.*;
import com.example.hw9.service.exceprtion.DataExistingException;
import com.example.hw9.service.exceprtion.NoDataFoundException;
import com.example.hw9.service.repository.EmployeeProjectHistoryRepository;
import com.example.hw9.service.repository.ManagerRepository;
import com.example.hw9.service.repository.ProjectRepository;
import com.example.hw9.service.repository.TimeInCompanyRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class HistoryController {

  private final EmployeeProjectHistoryRepository employeeProjectRepository;
  private final TimeInCompanyRepository timeInCompanyRepository;
  private final ManagerRepository managerRepository;
  private final ProjectRepository projectRepository;

  private void checkOnException(int size) {
    if (size == 0) {
      throw new NoDataFoundException("Data not found");
    }
  }
  // 1 my
  // a list of employees working on a given project on a given date

  @GetMapping("/employees/project/{projectId}/in/date/{date}")
  public String getEmployeeInProjectsInDate(Model model, @PathVariable Date date, @PathVariable Long projectId) {
    List<EmployeeProjectHistory> projectHistories;
    projectHistories = employeeProjectRepository.getEmployeeProjectByTimeStartLessThanEqualAndTimeLeftIsNullOrTimeLeftGreaterThanEqual(date, date);
    List<Employee> employees = projectHistories.stream()
        .filter(employeeProjectHistory -> employeeProjectHistory.getProjects().getId().equals(projectId))
        .map(EmployeeProjectHistory::getEmployees)
        .collect(Collectors.toList());
    Optional<Manager> manager = managerRepository.findManagerByProject_Id(projectId);
    employees.add(manager.get().getEmployee());
    distinct(employees);
    checkOnException(employees.size());
    model.addAttribute("employees", employees);
    return "employees";
  }


  // 2 my
  // list of employees working for the company, a specific date
  @GetMapping("/employees/in/date/{date}")
  public String getEmployeeWorkInDate(Model model, @PathVariable Date date) {
    List<TimeInCompany> timeInCompanies = timeInCompanyRepository.getTimeInCompanyByTimeHireLessThanEqualAndTimeSackNullOrTimeSackGreaterThanEqual(date, date);
    List<Employee> employees = getEmployeesFromTimeInCompany(timeInCompanies);
    checkOnException(employees.size());
    model.addAttribute("employees", employees);
    return "employees";
  }

  // 3 my
  // List of employees ever fired
  @GetMapping("/employees/fired")
  public String getEmployeeFired(Model model) {
    List<TimeInCompany> timeInCompanies = timeInCompanyRepository.getTimeInCompanyByTimeSackIsNotNull();
    List<Employee> employees = getEmployeesFromTimeInCompany(timeInCompanies);
    checkOnException(employees.size());
    model.addAttribute("employees", employees);
    return "employees";
  }

  // 4 my
  // a list of projects in which the employee was involved on a certain date
  @GetMapping("/projects/employee/{firstName}/{secondName}/was/{date}")
  public String getProjectEmployeeWasDate(Model model, @PathVariable Date date, @PathVariable String firstName, @PathVariable String secondName) {
    List<EmployeeProjectHistory> projectHistories = employeeProjectRepository.getEmployeeProjectHistoryByTimeStartIsLessThanEqualAndTimeLeftNullOrTimeLeftIsGreaterThanEqual(date, date);
    List<Project> projects = projectHistories.stream()
        .filter(employeeProjectHistory -> employeeProjectHistory.getEmployees().getFirstName().equals(firstName)
            && employeeProjectHistory.getEmployees().getSecondName().equals(secondName))
        .map(EmployeeProjectHistory::getProjects)
        .collect(Collectors.toList());
    checkOnException(projects.size());
    model.addAttribute("projects", projects);
    return "projects";
  }

  //5 my
  //projects started before the due date and not yet completed
  @GetMapping("/projects/start/before/{date}/not/completed")
  public String getProjectStartBeforeDateAndNotCompleted(Model model, @PathVariable Date date) {
    List<Project> projects = projectRepository.getProjectsByTimeStartIsLessThanEqualAndTimeFinishIsNull(date);
    checkOnException(projects.size());
    model.addAttribute("projects", projects);
    return "projects";
  }


  // 6 my
  // the number of projects in which the employee was involved before the dismissal
  @GetMapping("/projects/count/was/employee/{firstName}/{secondName}/before/fire")
  @ResponseBody
  public int getCountProjectsInWhichEmployeeWasInvolvedBeforeFire(@PathVariable String firstName, @PathVariable String secondName) {
    Optional<TimeInCompany> timeInCompany = timeInCompanyRepository.findTimeInCompanyByTimeSackIsNotNullAndEmployee_FirstNameAndEmployee_SecondName(firstName, secondName);
    if (timeInCompany.isEmpty()) {
      throw new DataExistingException("This employee not fire or not exists");
    }
    Employee employee = timeInCompany.get().getEmployee();
    Date date = timeInCompany.get().getTimeSack();
    return employeeProjectRepository.countAllByEmployeesAndTimeLeftLessThanEqual(employee, date);
  }

  private List<Employee> getEmployeesFromTimeInCompany(List<TimeInCompany> timeInCompanies) {
    checkOnException(timeInCompanies.size());
    List<Employee> employees = timeInCompanies.stream()
        .map(TimeInCompany::getEmployee)
        .collect(Collectors.toList());
    employees = distinct(employees);
    return employees;
  }


  private List<Employee> distinct(List<Employee> objects) {
    return objects.stream()
        .distinct()
        .collect(Collectors.toList());
  }

}
