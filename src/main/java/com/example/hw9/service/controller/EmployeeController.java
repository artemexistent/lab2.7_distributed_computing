package com.example.hw9.service.controller;

import com.example.hw9.model.database.*;
import com.example.hw9.service.exceprtion.DataExistingException;
import com.example.hw9.service.exceprtion.NoDataFoundException;
import com.example.hw9.service.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class EmployeeController {

  private final EmployeeRepository employeeRepository;
  private final ProjectRepository projectRepository;
  private final ManagerRepository managerRepository;
  private final DepartmentRepository departmentRepository;
  private final EmployeeProjectHistoryRepository employeeProjectRepository;


  @GetMapping("/employees")
  public String getEmployees(Model model) {
    List<Employee> employees = employeeRepository.findAll();
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    model.addAttribute("employees", employees);
    return "employees";
  }

  @GetMapping("/employee/{firstName}/{secondName}")
  public String getEmployeeInfo(Model model, @PathVariable String firstName, @PathVariable String secondName) {
    Optional<Employee> employee = employeeRepository.findEmployeeByFirstNameAndSecondName(firstName, secondName);
    if (employee.isEmpty()) {
      throw new NoDataFoundException("Employee not found");
    }
    model.addAttribute("employees", employee);
    return "employees";
  }

  @GetMapping("employees/department/{departmentName}")
  public String getEmployeesFromDepartment(@PathVariable String departmentName, Model model) {
    Optional<Department> department = departmentRepository.findByName(departmentName);
    if (department.isEmpty()) {
      throw new NoDataFoundException("Department not found");
    }
    List<Employee> employees = employeeRepository.getEmployeesByDepartment(department.get());
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    model.addAttribute("employees", employees);

    return "employees";
  }


  // 1 a list of employees working on a given project
  @GetMapping("employees/project/{projectId}")
  public String getEmployeesProject(@PathVariable Long projectId, Model model) {
    Optional<Project> project = projectRepository.findById(projectId);
    if (project.isEmpty()) {
      throw new NoDataFoundException("Project not found");
    }
    List<Employee> employees = getEmployeesWhoAreInProjects(Collections.singletonList(project.get()));
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    model.addAttribute("employees", employees);
    return "employees";
  }

  // 3 a list of employees from a given department who are not involved in any project
  @GetMapping("/employees/department/{departmentName}/free")
  public String getEmployeesFromDepartmentAreFree(@PathVariable String departmentName, Model model) {
    Optional<Department> department = departmentRepository.findByName(departmentName);
    if (department.isEmpty()) {
      throw new NoDataFoundException("Department not found");
    }
    List<Employee> employees = employeeRepository.getEmployeesByDepartmentAndProjectsIsNull(department.get());
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    getEmployeeWithoutManager(employees);
    model.addAttribute("employees", employees);

    return "employees";
  }

  // 4 list of employees who are not involved in any project
  @GetMapping("/employees/free")
  public String getEmployeesAreFree(Model model) {
    List<Employee> employees = employeeRepository.getEmployeesByProjectsIsNull();
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    getEmployeeWithoutManager(employees);
    model.addAttribute("employees", employees);
    return "employees";
  }

  //5 list of subordinates for a given manager (for all projects, he is in charge)
  @GetMapping("employees/manager/{managerFirstName}/{managerSecondName}")
  public String getManagerEmployee(Model model, @PathVariable String managerFirstName, @PathVariable String managerSecondName) {
    List<Manager> manager = managerRepository.getManagersByEmployee_FirstNameAndEmployee_SecondName(managerFirstName, managerSecondName);
    if (manager.size() == 0) {
      throw new NoDataFoundException("Employees is not manager");
    }
    List<Project> projects = manager.stream()
        .map(Manager::getProject)
        .collect(Collectors.toList());
    List<Employee> employees = employeeRepository.getEmployeesByProjectsIsIn(projects);
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    employees = distinct(employees);
    model.addAttribute("employees", employees);
    return "employees";
  }

  // 7 list of employees participating in the same projects as a given employee
  @GetMapping("/employees/the/same/project/employee/{firstName}/{secondName}")
  public String getEmployeeFromTheSameProject(Model model, @PathVariable String firstName, @PathVariable String secondName) {

    Optional<Employee> employee = employeeRepository.findEmployeeByFirstNameAndSecondName(firstName, secondName);
    if (employee.isEmpty()) {
      throw new NoDataFoundException("Employees not found");
    }
    List<Project> projects = employee.get().getProjects();
    List<Employee> employeesWhoAreInProjects = getEmployeesWhoAreInProjects(projects);
    List<Manager> employeeIsManager = managerRepository.getManagersByEmployee_FirstNameAndEmployee_SecondName(firstName, secondName);
    List<Project> managerProjects = employeeIsManager.stream()
        .map(Manager::getProject)
        .collect(Collectors.toList());
    List<Employee> finalEmployee = employeeRepository.getEmployeesByProjectsIsIn(managerProjects);
    finalEmployee.addAll(employeesWhoAreInProjects);
    if (finalEmployee.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    finalEmployee = distinct(finalEmployee);
    model.addAttribute("employees", finalEmployee);

    return "employees";
  }

  // 9 list of employees involved in projects carried out for a given customer
  @GetMapping("/employees/project/customer/{customerCompany}")
  public String getEmployeeInProjectsFromCustomer(@PathVariable String customerCompany, Model model) {
    List<Project> projects = projectRepository.getProjectsByCustomer_Company(customerCompany);
    List<Employee> employees = getEmployeesWhoAreInProjects(projects);
    if (employees.size() == 0) {
      throw new NoDataFoundException("Employees not found");
    }
    employees = distinct(employees);
    model.addAttribute("employees", employees);

    return "employees";
  }



  @PostMapping("/employee/create")
  public String createEmployee(Model model, @RequestBody Employee employee) {
    if (employeeRepository.existsByFirstNameAndSecondName(employee.getFirstName(), employee.getSecondName())) {
      throw new DataExistingException("This employee existing");
    }
    TimeInCompany time = new TimeInCompany(null, employee, Date.valueOf(LocalDate.now()), null);
    employee.setTimeInCompany(Collections.singletonList(time));
    getDepartmentAndSaveEmployee(employee);
    model.addAttribute("employees", employee);
    return "employees";
  }

  @PutMapping("/employee/update")
  @ResponseBody
  public String updateEmployee(@RequestBody Employee employee) {
    Optional<Employee> employeeFromDataBase = employeeRepository.findEmployeeByFirstNameAndSecondName(employee.getFirstName(), employee.getSecondName());
    if (employeeFromDataBase.isEmpty()) {
      throw new DataExistingException("This employee not existing");
    }
    Employee employeeDB = employeeFromDataBase.get();
    employeeDB.setPosition(employee.getPosition());
    employeeDB.setDepartment(employee.getDepartment());
    getDepartmentAndSaveEmployee(employeeDB);
    return "Success";
  }

  @PutMapping("/employee/{firstName}/{secondName}/set/project/{projectId}")
  @ResponseBody
  public String setEmployeeProject(@PathVariable Long projectId, @PathVariable String firstName, @PathVariable String secondName) {
    Employee employee = checkDataForEmployeeProjectAndGetEmployee(firstName, secondName, projectId);
    Optional<Project> project = projectRepository.findById(projectId);
    if (employeeRepository.existsByFirstNameAndSecondNameAndProjectsIsIn(firstName, secondName, Collections.singletonList(project.get()))) {
      throw new DataExistingException("This employee is in this project");
    }
    EmployeeProjectHistory projectHistory = new EmployeeProjectHistory(null, employee, project.get(), Date.valueOf(LocalDate.now()), null);
    employeeProjectRepository.saveAndFlush(projectHistory);
    return "Success";
  }

  @PutMapping("/employee/{firstName}/{secondName}/fire")
  @ResponseBody
  public String fireEmployee(@PathVariable String firstName, @PathVariable String secondName) {
    Optional<Employee> employeeInDataBase = employeeRepository.findEmployeeByFirstNameAndSecondName(firstName, secondName);
    if (employeeInDataBase.isEmpty()) {
      throw new DataExistingException("This employee not found");
    }
    Employee employee = employeeInDataBase.get();
    employee.getProjects().clear();
    int sizeTime = employee.getTimeInCompany().size();
    if (employee.getTimeInCompany().get(sizeTime - 1).getTimeSack() != null) {
      throw new DataExistingException("This employee not hire");
    }
    employee.getTimeInCompany().get(sizeTime - 1).setTimeSack(Date.valueOf(LocalDate.now()));
    employeeRepository.saveAndFlush(employee);
    return "Success";
  }

  @PutMapping("/employee/{firstName}/{secondName}/hire")
  @ResponseBody
  public String hireEmployee(@PathVariable String firstName, @PathVariable String secondName) {
    Optional<Employee> employeeInDataBase = employeeRepository.findEmployeeByFirstNameAndSecondName(firstName, secondName);
    if (employeeInDataBase.isEmpty()) {
      throw new DataExistingException("This employee not found");
    }
    Employee employee = employeeInDataBase.get();
    int sizeTime = employee.getTimeInCompany().size();
    if (employee.getTimeInCompany().get(sizeTime - 1).getTimeHire() != null && employee.getTimeInCompany().get(sizeTime - 1).getTimeSack() == null) {
      throw new DataExistingException("This employee not fire");
    }
    employee.getTimeInCompany().add(new TimeInCompany(null, employee, Date.valueOf(LocalDate.now()), null));
    employeeRepository.saveAndFlush(employee);
    return "Success";
  }

  @DeleteMapping("/employee/{firstName}/{secondName}/delete")
  @ResponseBody
  public String deleteEmployee(@PathVariable String firstName, @PathVariable String secondName) {
    Optional<Employee> employee = employeeRepository.findEmployeeByFirstNameAndSecondName(firstName, secondName);
    if (employee.isEmpty()) {
      throw new DataExistingException("This employee not existing");
    }
    if (managerRepository.existsByEmployee_FirstNameAndEmployee_SecondName(firstName, secondName)) {
      throw new DataExistingException("This employee is manager");
    }
    employee.get().getProjects().clear();
    employeeRepository.saveAndFlush(employee.get());
    employeeRepository.delete(employee.get());
    return "Success";
  }

  @DeleteMapping("/employee/{firstName}/{secondName}/delete/project/{projectId}")
  @ResponseBody
  public String deleteEmployeeFromProject(@PathVariable String firstName, @PathVariable String secondName, @PathVariable Long projectId) {

    Employee employee = checkDataForEmployeeProjectAndGetEmployee(firstName, secondName, projectId);
    Optional<Project> project = projectRepository.findById(projectId);
    if (!employeeRepository.existsByFirstNameAndSecondNameAndProjectsIsIn(firstName, secondName, Collections.singletonList(project.get()))) {
      throw new DataExistingException("This employee is not in this project");
    }
    employee.getProjects().remove(project.get());
    employeeRepository.saveAndFlush(employee);
    return "Success";
  }

  private Employee checkDataForEmployeeProjectAndGetEmployee(String firstName, String secondName, Long projectId) {
    Optional<Employee> employeeInDataBase = employeeRepository.findEmployeeByFirstNameAndSecondName(firstName, secondName);
    if (employeeInDataBase.isEmpty()) {
      throw new DataExistingException("This employee not existing");
    }
    Optional<Project> project = projectRepository.findById(projectId);
    if (project.isEmpty()) {
      throw new DataExistingException("This project not found");
    }
    if (managerRepository.existsByEmployee_FirstNameAndEmployee_SecondNameAndProject_Id(firstName, secondName, projectId)) {
      throw new DataExistingException("This employee is manager this project");
    }
    return employeeInDataBase.get();
  }

  private void getDepartmentAndSaveEmployee(Employee employee) {
    String departmentName = employee.getDepartment().getName();
    Optional<Department> department = departmentRepository.findByName(departmentName);
    if (department.isEmpty()) {
      throw new NoDataFoundException("Department not found");
    }
    employee.setDepartment(department.get());
    employeeRepository.saveAndFlush(employee);
  }

  private void getEmployeeWithoutManager(List<Employee> employees) {
    List<Manager> managers = managerRepository.findAll();
    List<Employee> employeeManager = managers.stream()
        .map(Manager::getEmployee)
        .collect(Collectors.toList());
    employees.removeAll(employeeManager);
  }

  private List<Employee> getEmployeesWhoAreInProjects(List<Project> projects) {
    List<Employee> employees = employeeRepository.getEmployeesByProjectsIsIn(projects);
    List<Manager> managers = managerRepository.getManagersByProjectIsIn(projects);
    List<Employee> employeeManager = managers.stream()
        .map(Manager::getEmployee)
        .collect(Collectors.toList());
    employees.addAll(employeeManager);
    employees = distinct(employees);
    return employees;
  }

  private List<Employee> distinct(List<Employee> objects) {
    return objects.stream()
        .distinct()
        .collect(Collectors.toList());
  }
}
