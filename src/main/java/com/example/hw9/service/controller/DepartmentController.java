package com.example.hw9.service.controller;

import com.example.hw9.model.database.Department;
import com.example.hw9.service.exceprtion.DataExistingException;
import com.example.hw9.service.exceprtion.NoDataFoundException;
import com.example.hw9.service.repository.DepartmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
public class DepartmentController {

  private DepartmentRepository departmentRepository;

  @GetMapping("/departments")
  public String getDepartments(Model model) {
    List<Department> departments = departmentRepository.findAll();
    if (departments.size() == 0) {
      throw new NoDataFoundException("Departments not found ");
    }
    model.addAttribute("departments", departments);
    return "departments";
  }

  @GetMapping("/department/{departmentName}")
  public String getDepartmentInfo(@PathVariable String departmentName, Model model) {
    Optional<Department> department = departmentRepository.findByName(departmentName);
    if (department.isEmpty()) {
      throw new NoDataFoundException("Department not found");
    }
    model.addAttribute("departments", department);
    return "departments";
  }

  @PostMapping("/department/create")
  public String createDepartment(Model model, @RequestBody Department department) {
    if (departmentRepository.existsByName(department.getName())) {
      throw new DataExistingException("This department existing");
    }
    departmentRepository.saveAndFlush(department);
    model.addAttribute("departments", department);
    return "departments";
  }

  @PutMapping("/department/update")
  @ResponseBody
  public String updateDepartment(@RequestBody Department department) {
    Optional<Department> departmentInDataBase = departmentRepository.findByName(department.getName());
    if (departmentInDataBase.isEmpty()) {
      throw new DataExistingException("This department not existing");
    }
    Long departmentId = departmentInDataBase.get().getId();
    department.setId(departmentId);
    departmentRepository.saveAndFlush(department);
    return "Success";
  }

  @DeleteMapping("/department/{departmentName}/delete")
  @ResponseBody
  public String deleteDepartment(@PathVariable String departmentName) {
    Optional<Department> departmentInDataBase = departmentRepository.findByName(departmentName);
    if (departmentInDataBase.isEmpty()) {
      throw new DataExistingException("This department not existing");
    }
    Department department = departmentInDataBase.get();
    try {
      departmentRepository.delete(department);
    } catch (Exception ex) {
      throw new DataExistingException("in this department exists employee");
    }
    return "Success";
  }

}
