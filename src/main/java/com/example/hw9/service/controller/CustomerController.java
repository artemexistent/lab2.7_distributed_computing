package com.example.hw9.service.controller;

import com.example.hw9.model.database.Customer;
import com.example.hw9.service.exceprtion.DataExistingException;
import com.example.hw9.service.exceprtion.NoDataFoundException;
import com.example.hw9.service.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
public class CustomerController {

  private final CustomerRepository customerRepository;


  @GetMapping("/customers")
  public String getCustomers(Model model) {
    List<Customer> customers = customerRepository.findAll();
    if (customers.size() == 0) {
      throw new NoDataFoundException("Customers not found ");
    }
    model.addAttribute("customers", customers);
    return "customers";
  }

  @GetMapping("/customer/{customerCompany}")
  public String getCustomerInfo(Model model, @PathVariable String customerCompany) {
    Optional<Customer> customer = customerRepository.findByCompany(customerCompany);
    if (customer.isEmpty()) {
      throw new NoDataFoundException("Customer not found ");
    }
    model.addAttribute("customers", customer);
    return "customers";
  }

  @PostMapping("/customer/create")
  public String createCustomer(Model model, @RequestBody Customer customer) {
    if (customerRepository.existsByCompany(customer.getCompany())) {
      throw new DataExistingException("This customer existing");
    }
    customerRepository.saveAndFlush(customer);
    model.addAttribute("customers", customer);
    return "customers";
  }

  @PutMapping("/customer/update")
  @ResponseBody
  public String updateCustomer(@RequestBody Customer customer) {
    Optional<Customer> customerInDataBase = customerRepository.findByCompany(customer.getCompany());
    if (customerInDataBase.isEmpty()) {
      throw new DataExistingException("This customer not existing");
    }
    Long customerId = customerInDataBase.get().getId();
    customer.setId(customerId);
    customerRepository.saveAndFlush(customer);
    return "Success";
  }

  @DeleteMapping("/customer/{customerCompany}/delete")
  @ResponseBody
  public String deleteCustomer(@PathVariable String customerCompany) {
    Optional<Customer> customerInDataBase = customerRepository.findByCompany(customerCompany);
    if (customerInDataBase.isEmpty()) {
      throw new DataExistingException("This customer not existing");
    }
    Customer customer = customerInDataBase.get();
    try {
      customerRepository.delete(customer);
    } catch (Exception ex) {
      throw new DataExistingException("Exists project from this customer");
    }
    return "Success";
  }
}
