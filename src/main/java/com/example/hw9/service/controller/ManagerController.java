package com.example.hw9.service.controller;

import com.example.hw9.model.database.Employee;
import com.example.hw9.model.database.Manager;
import com.example.hw9.model.database.Project;
import com.example.hw9.service.exceprtion.DataExistingException;
import com.example.hw9.service.exceprtion.NoDataFoundException;
import com.example.hw9.service.repository.EmployeeRepository;
import com.example.hw9.service.repository.ManagerRepository;
import com.example.hw9.service.repository.ProjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class ManagerController {


  private EmployeeRepository employeeRepository;

  private ManagerRepository managerRepository;

  private ProjectRepository projectRepository;

  @GetMapping("/managers")
  public String getManagers(Model model) {
    List<Manager> managers = managerRepository.findAll();
    if (managers.size() == 0) {
      throw new NoDataFoundException("Managers not found");
    }
    model.addAttribute("managers", managers);
    return "managers";
  }

  @GetMapping("/manager/{managerFirstName}/{managerSecondName}")
  public String getManagerInfo(Model model, @PathVariable String managerFirstName, @PathVariable String managerSecondName) {
    List<Manager> manager = managerRepository.getManagersByEmployee_FirstNameAndEmployee_SecondName(managerFirstName, managerSecondName);
    if (manager.size() == 0) {
      throw new NoDataFoundException("Manager not found");
    }
    model.addAttribute("managers", manager);
    return "managers";
  }

  //6 list of managers for a given employee (for all projects in which he participates)
  @GetMapping("managers/employee/{employeeFirstName}/{employeeSecondName}")
  public String getEmployeeManager(Model model, @PathVariable String employeeFirstName, @PathVariable String employeeSecondName) {
    Optional<Employee> employee = employeeRepository.findEmployeeByFirstNameAndSecondName(employeeFirstName, employeeSecondName);
    if (employee.isEmpty()) {
      throw new NoDataFoundException("Employee not found");
    }
    List<Project> projects = employee.get().getProjects();
    List<Manager> managers = managerRepository.getManagersByProjectIsIn(projects);
    if (managers.size() == 0) {
      throw new NoDataFoundException("Managers not found");
    }
    managers = distinct(managers);
    model.addAttribute("managers", managers);
    return "managers";
  }

  @PostMapping("/manager/create")
  public String createDepartment(Model model, @RequestBody Manager manager) {
    Employee employee = manager.getEmployee();
    Optional<Employee> employeeInDataBase = employeeRepository
        .findEmployeeByFirstNameAndSecondName(employee.getFirstName(), employee.getSecondName());
    if (employeeInDataBase.isEmpty()) {
      throw new DataExistingException("This manager existing");
    }
    Long projectId = manager.getProject().getId();
    if (managerRepository.existsByProjectId(projectId)) {
      throw new DataExistingException("This project has manager");
    }
    if (!employeeRepository.existsByFirstNameAndSecondName(employee.getFirstName(), employee.getSecondName())) {
      throw new DataExistingException("This employee not found");
    }
    Optional<Project> project = projectRepository.findById(projectId);
    if (project.isEmpty()) {
      throw new NoDataFoundException("This project not found");
    }
    manager.setProject(project.get());
    manager.setEmployee(employeeInDataBase.get());
    managerRepository.saveAndFlush(manager);
    model.addAttribute("managers", manager);
    return "managers";
  }

  @PutMapping("/manager/update")
  @ResponseBody
  public String updateDepartment(@RequestBody Manager manager) {
    Employee employee = manager.getEmployee();
    List<Manager> managerInDataBase = managerRepository
        .getManagersByEmployee_FirstNameAndEmployee_SecondName(employee.getFirstName(), employee.getSecondName());
    if (managerInDataBase.size() == 0) {
      throw new DataExistingException("This manager not existing");
    }
    Long projectId = manager.getProject().getId();
    if (managerRepository.existsByProjectId(projectId)) {
      throw new DataExistingException("This project has manager");
    }
    Optional<Employee> employeeInDataBase = employeeRepository.findEmployeeByFirstNameAndSecondName(employee.getFirstName(), employee.getSecondName());
    if (employeeInDataBase.isEmpty()) {
      throw new DataExistingException("This employee not found");
    }
    Optional<Project> project = projectRepository.findById(projectId);
    if (project.isEmpty()) {
      throw new NoDataFoundException("This project not found");
    }
    manager.setEmployee(employeeInDataBase.get());
    manager.setProject(project.get());
    managerRepository.saveAndFlush(manager);
    return "Success";
  }

  @DeleteMapping("/manager/{managerFirstName}/{managerSecondName}/delete")
  @ResponseBody
  @Transient
  public String deleteDepartment(@PathVariable String managerFirstName, @PathVariable String managerSecondName) {
    List<Manager> managers = managerRepository.getManagersByEmployee_FirstNameAndEmployee_SecondName(managerFirstName, managerSecondName);
    if (managers.size() == 0) {
      throw new DataExistingException("This manager not existing");
    }
    managerRepository.deleteAll(managers);
    return "Success";
  }


  private List<Manager> distinct(List<Manager> objects) {
    return objects.stream()
        .distinct()
        .collect(Collectors.toList());
  }

}
