package com.example.hw9.service.controller;

import com.example.hw9.model.database.Customer;
import com.example.hw9.model.database.Employee;
import com.example.hw9.model.database.Manager;
import com.example.hw9.model.database.Project;
import com.example.hw9.service.exceprtion.DataExistingException;
import com.example.hw9.service.exceprtion.NoDataFoundException;
import com.example.hw9.service.repository.CustomerRepository;
import com.example.hw9.service.repository.EmployeeRepository;
import com.example.hw9.service.repository.ManagerRepository;
import com.example.hw9.service.repository.ProjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class ProjectController {

  private final EmployeeRepository employeeRepository;
  private final ProjectRepository projectRepository;
  private final ManagerRepository managerRepository;
  private final CustomerRepository customerRepository;


  @GetMapping("/projects")
  public String getProjects(Model model) {
    List<Project> projects = projectRepository.findAll();
    if (projects.size() == 0) {
      throw new NoDataFoundException("Projects not found ");
    }
    model.addAttribute("projects", projects);
    return "projects";
  }


  @GetMapping("/project/{id}")
  public String getProjectInfo(@PathVariable Long id, Model model) {
    Optional<Project> project = projectRepository.findById(id);
    if (project.isEmpty()) {
      throw new NoDataFoundException("Project not found ");
    }
    model.addAttribute("projects", project);
    return "projects";
  }


  // 2 list of projects in which a given employee participates
  @GetMapping("project/employee/{employeeFirstName}/{employeeSecondName}")
  public String getProjectEmployee(Model model, @PathVariable String employeeFirstName, @PathVariable String employeeSecondName) {

    Optional<Employee> employeeInDataBase = employeeRepository.findEmployeeByFirstNameAndSecondName(employeeFirstName, employeeSecondName);
    if (employeeInDataBase.isEmpty()) {
      throw new NoDataFoundException("Employee not found");
    }
    Employee employee = employeeInDataBase.get();
    List<Manager> managers = managerRepository
        .getManagersByEmployee_FirstNameAndEmployee_SecondName(employee.getFirstName(), employee.getSecondName());
    List<Project> projects = employee.getProjects();
    List<Project> managersProjects = managers.stream()
        .map(Manager::getProject)
        .collect(Collectors.toList());
    projects.addAll(managersProjects);
    if (projects.size() == 0) {
      throw new NoDataFoundException("Projects not found");
    }
    model.addAttribute("projects", projects);

    return "projects";
  }

  //8 list of projects carried out for a given customer
  @GetMapping("projects/customer/{customerCompany}")
  public String getProjectFromCustomer(@PathVariable String customerCompany, Model model) {
    List<Project> projects = projectRepository.getProjectsByCustomer_Company(customerCompany);
    if (projects.size() == 0) {
      throw new NoDataFoundException("Projects not found");
    }
    model.addAttribute("projects", projects);
    return "projects";
  }

  @PostMapping("/project/create")
  public String createProject(Model model, @RequestBody Project project) {
    if (projectRepository.existsById(project.getId())) {
      throw new DataExistingException("This project existing");
    }
    checkAndSetCustomer(project);
    project.setTimeStart(Date.valueOf(LocalDate.now()));
    projectRepository.saveAndFlush(project);
    model.addAttribute("projects", project);
    return "projects";
  }

  @PutMapping("/project/update")
  @ResponseBody
  public String updateProject(@RequestBody Project project) {
    Optional<Project> projectOptional = projectRepository.findById(project.getId());
    if (projectOptional.isEmpty()) {
      throw new DataExistingException("This project not existing");
    }
    projectOptional.get().setCustomer(project.getCustomer());
    projectOptional.get().setInfo(project.getInfo());
    checkAndSetCustomer(projectOptional.get());
    projectRepository.saveAndFlush(projectOptional.get());
    return "Success";
  }

  @PutMapping("/project/{projectId}/finish")
  @ResponseBody
  public String finishProject(@PathVariable Long projectId) {
    Optional<Project> projectInDataBase = projectRepository.findById(projectId);
    if (projectInDataBase.isEmpty()) {
      throw new DataExistingException("This project not found");
    }
    Project project = projectInDataBase.get();
    if (project.getTimeFinish() != null) {
      throw new DataExistingException("This project was finished");
    }
    project.setTimeFinish(Date.valueOf(LocalDate.now()));
    projectRepository.saveAndFlush(project);
    return "Success";
  }

  @DeleteMapping("/project/{id}/delete")
  @ResponseBody
  @Transactional
  public String deleteProject(@PathVariable Long id) {
    Optional<Project> project = projectRepository.findById(id);
    if (project.isEmpty()) {
      throw new DataExistingException("This project not existing");
    }
    List<Employee> employees = employeeRepository.getEmployeesByProjectsIsIn(Collections.singletonList(project.get()));
    employees.forEach(employee -> employee.getProjects().remove(project.get()));
    projectRepository.deleteById(id);
    return "Success";
  }

  private void checkAndSetCustomer(@RequestBody Project project) {
    String company = project.getCustomer().getCompany();
    Optional<Customer> customer = customerRepository.findByCompany(company);
    if (customer.isEmpty()) {
      throw new DataExistingException("Customer not found");
    }
    project.setCustomer(customer.get());
  }

}
