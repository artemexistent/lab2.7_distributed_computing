package com.example.hw9.model.database;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "employee")
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "firstname")
  private String firstName;
  @Column(name = "secondname")
  private String secondName;
  @Column(name = "position")
  private String position;

  @ManyToOne(optional = false, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_department", referencedColumnName = "id")
  private Department department;

  @EqualsAndHashCode.Exclude
  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(
      name = "employee_project",
      joinColumns = @JoinColumn(name = "id_employee", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "id_project", referencedColumnName = "id"))
  private List<Project> projects;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "employee", orphanRemoval = true)
  private List<TimeInCompany> timeInCompany;

}
