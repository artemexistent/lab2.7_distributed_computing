package com.example.hw9.model.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "time_in_company")
public class TimeInCompany {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(optional = false, cascade = CascadeType.REMOVE)
  @JoinColumn(name = "id_employee", referencedColumnName = "id")
  private Employee employee;

  @Column(name = "time_hire")
  private Date timeHire;
  @Column(name = "time_sack")
  private Date timeSack;


}
