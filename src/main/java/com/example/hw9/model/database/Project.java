package com.example.hw9.model.database;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "project")
public class Project {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "info")
  private String info;

  @Column(name = "time_start")
  private Date timeStart;
  @Column(name = "time_finish")
  private Date timeFinish;

  @ManyToOne
  @JoinColumn(name = "id_customer", referencedColumnName = "id")
  private Customer customer;
}
