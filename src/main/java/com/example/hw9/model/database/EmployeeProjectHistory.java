package com.example.hw9.model.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee_project")
public class EmployeeProjectHistory {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(optional = false, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_employee", referencedColumnName = "id")
  private Employee employees;

  @ManyToOne(optional = false, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_project", referencedColumnName = "id")
  private Project projects;

  @Column(name = "time_start")
  private Date timeStart;
  @Column(name = "time_left")
  private Date timeLeft;
}
