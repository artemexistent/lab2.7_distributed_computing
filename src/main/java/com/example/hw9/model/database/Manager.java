package com.example.hw9.model.database;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "manager")
public class Manager {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "id_employee", referencedColumnName = "id")
  private Employee employee;

  @EqualsAndHashCode.Exclude
  @OneToOne
  @JoinColumn(name = "id_project", referencedColumnName = "id")
  private Project project;


}
